import sys
a = sys.argv[1]
prev = 0
nodes = []
flag = 0

for n, char in enumerate(a):
  if char == '[':
    flag = 1
  if char == ']':
    flag = 0
  if (char == ',' and flag == 0):
    nodes.append(a[prev:n])
    prev = n+1
  if n == len(a)-1:
    nodes.append(a[prev:n+1])

def split_coma(node):
  result = []
  if '[' in node:
    core = node.split('[')[0]
    ranges = node.split('[')[1].strip(']').split(',')
    for r in ranges:
      if '-' in r:
        result.extend(list(range(int(r.split('-')[0]), int(r.split('-')[1])+1)))
      else:
        result.append(int(r))
    result = [core + "{:02d}".format(r) for r in result]
  else:
    result.append(node)
  return result

print(' '.join([n for ns in nodes for n in split_coma(ns)]))
