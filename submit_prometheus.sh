#!/bin/bash -l
#SBATCH -J crooks
#SBATCH -N 18
#SBATCH -n 432
#SBATCH --ntasks-per-node=24
#SBATCH --time=12:00:00

cd $SLURM_SUBMIT_DIR

module load plgrid/apps/gromacs/2018.2

mpiexec -n 432 /net/people/$USER/anaconda3/bin/python -m mpi4py.futures ./crooks.py -f ../mygro.gro --fxtc ../state0.xtc --gxtc ../state1.xtc -p ../topol.top -n 215 -t 1000 -g gmx --maxwarn 2  -a nacl