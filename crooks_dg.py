import argparse
from concurrent.futures import ProcessPoolExecutor
import os
import sys
import numpy as np
from scipy.integrate import simps
from sklearn.neighbors import KernelDensity
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import LeaveOneOut
import glob
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


class CrooksPool:
    def __init__(self, length=500, convergence=False, alias='free', sel_d0='run*0', sel_d1='run*1'):
        """
        A pool of workers to collect the data
        :param nruns: int, number of A->B runs (overall, twice this many runs will be performed)
        :param length: int, length of a single trajectory (in ps)
        :param convergence: bool, whether to do convergence analysis
        :param alias: str, a base name for all output files
        """
        self.name = alias
        self.bandwidth = 0
        self.convergence = convergence
        self.sim_length = length
        self.nst = self.sim_length * 500
        self.dirs_0 = glob.glob(sel_d0)
        self.dirs_1 = glob.glob(sel_d1)
        assert len(self.dirs_0) == len(self.dirs_1)
        self.nruns = len(self.dirs_0)
        self.workers = [Crooks(self, n, lam) for n in range(self.nruns) for lam in [0, 1]]

    def analyze(self):
        """
        The analysis routine: performed after the run,
        reads all data files, processes them and
        prints out the results and/or convergence data
        :return:
        """
        print('analyzing results...')
        for worker in self.workers:
            worker.analyze_me()
        works_0 = np.array([crk.work for crk in self.workers if crk.initlambda == 0]).reshape(-1, 1)
        works_1 = np.array([-crk.work for crk in self.workers if crk.initlambda == 1]).reshape(-1, 1)
        np.savetxt('work_0.dat', works_0, fmt='%10.5f')
        np.savetxt('work_1.dat', works_1, fmt='%10.5f')
        min_val = np.min(np.vstack([works_0, works_1]))
        max_val = np.max(np.vstack([works_0, works_1]))
        diff = max_val - min_val
        min_range = min_val - 0.1 * diff
        max_range = max_val + 0.1 * diff
        grid = np.linspace([min_range], [max_range], 200).reshape(-1, 1)
        dens0 = self.get_opt_kde(works_0, grid)
        dens1 = self.get_opt_kde(works_1, grid)
        np.savetxt('prob_0.dat', np.hstack([grid, dens0]), fmt='%10.5f')
        np.savetxt('prob_1.dat', np.hstack([grid, dens1]), fmt='%10.5f')
        result_kde = self.solve_kde(dens0, dens1, grid)
        result_cgi = self.solve_cgi(works_0, works_1)
        self.plot_results(works_0, works_1, np.hstack([grid, dens0]), np.hstack([grid, dens1]), result_kde, result_cgi)
        np.savetxt('normalized_overlap_{}.dat'.format(self.name), self.bhattacharyya(dens0, dens1, grid),
                   fmt='%10.5f')
        if self.convergence:
            self.analyze_convergence(works_0, works_1, grid, 'kde')
            self.analyze_convergence(works_0, works_1, grid, 'cgi')

    @staticmethod
    def bhattacharyya(d0, d1, grid):
        """
        Calculates the Bhattacharyya (similarity) coefficient between distributions d0 and d1
        :param d0: np.array, rho_0, density of A->B work values
        :param d1: np.array, rho_1, density of B->A -work values
        :param grid: np.array, the corresponding X values
        :return: np.array of shape [1,1]
        """
        return simps(np.sqrt(d0.reshape(-1) * d1.reshape(-1)), grid.reshape(-1)).reshape(-1, 1)

    def analyze_convergence(self, w0, w1, grid, label):
        """
        Performs a series of bootstraps for a number of
        subsample sizes, then plots the resulting means/
        standard deviations to a file
        :param w0: np.array, work values for A->B
        :param w1: np.array, -work values for A->B
        :param grid: np.array, the X values, as in the original plot
        :return: None
        """
        samples = np.linspace(4, self.nruns, 10).astype(int)
        conv = {ns: [] for ns in samples}
        # if self.debug:
        #     try:
        #         os.mkdir('bootstrap')
        #     except FileExistsError:
        #         pass
        for nsamp in samples:
            print('bootstrap for {} samples...'.format(nsamp))
            with ProcessPoolExecutor(max_workers=10) as executor:
                results = np.array(list(executor.map(self.boot, [(w0, w1, nsamp, grid, i, label)
                                                                 for i in range(10)]))).reshape(-1)
            conv[nsamp] = [np.mean(results), np.std(results)]
        plt.plot(samples, [conv[x][0] for x in samples])
        plt.errorbar(samples, [conv[x][0] for x in samples], yerr=[conv[x][1] for x in samples])
        plt.savefig('convergence_{}_{}.svg'.format(label, self.name))
        plt.close()

    def boot(self, params):
        """
        Performs a single bootstrap resampling,
        parallelized through ProcessPoolExecutor
        :param params: a 5-element tuple of input parameters:
        A->B work, B->A -work, number of subsamples to draw, original X values, random seed
        :return: float, the KDE solution for the intersection
        """
        w0, w1, nsamp, grid, n, runtype = params
        np.random.seed(n)
        w0_sampled = np.random.choice(w0.reshape(-1), nsamp, replace=True).reshape(-1, 1)
        w1_sampled = np.random.choice(w1.reshape(-1), nsamp, replace=True).reshape(-1, 1)
        if runtype == 'kde':
            dens0_sampled = self.get_opt_kde(w0_sampled, grid, self.bandwidth)
            dens1_sampled = self.get_opt_kde(w1_sampled, grid, self.bandwidth)
            result = self.solve_kde(dens0_sampled, dens1_sampled, grid)
        else:
            result = self.solve_cgi(w0_sampled, w1_sampled)
        # np.savetxt('bootstrap/conv_{}_{}_{}_{}.dat'.format(nsamp, n, str(result_kde), str(result_cgi)),
        #            np.hstack([grid, dens0_sampled, dens1_sampled]), fmt='%10.5f')
        return result

    @staticmethod
    def solve_kde(d0, d1, grid):
        """
        Finds the intersection point based
        on KDE densities
        :param d0: np.array,
        :param d1: np.array,
        :param grid: np.array,
        :return: float, abscissa of intersection
        """
        diff = d0 - d1
        avg = 0.5 * (np.argmax(d0) + np.argmax(d1))
        solution = [x for x in np.where(diff[1:] * diff[:-1] < 0)[0]]
        arg = solution[np.argmin([np.abs(x - avg) for x in solution])]
        result = round(0.5 * (grid.reshape(-1)[arg] + grid.reshape(-1)[arg]), 3)
        return result

    @staticmethod
    def solve_cgi(w0, w1):
        """
        Finds the intersection point based on the CGI
        estimator (intersection of two Gaussians)
        :param w0: np.array, work values for A->B
        :param w1: np.array, -work values for B->A
        :return: float, abscissa of intersection
        """
        w0, w1 = w0.reshape(-1), w1.reshape(-1)
        m0, s0 = np.mean(w0), np.std(w0)
        m1, s1 = np.mean(w1), np.std(w1)
        x1 = m0 / s0 ** 2 - m1 / s1 ** 2
        den = 1 / s0 ** 2 - 1 / s1 ** 2
        sq = (m0 - m1) ** 2 / (s0 ** 2 * s1 ** 2) + 2 * den * np.log(s1 / s0)
        mid = (m0 + m1) / 2
        r1, r2 = (x1 - np.sqrt(sq)) / den, (x1 + np.sqrt(sq)) / den
        return round(r1, 3) if np.abs(r1 - mid) < np.abs(r2 - mid) else round(r2, 3)

    def get_opt_kde(self, y, x, bandwidth=None):
        """
        Performs KDE (kernel density estimation) with a Gaussian
        kernel, and optimizes the bandwidth using the LeaveOneOut
        cross-validation if requested
        :param y: np.array, work values
        :param x: np.array, X values for the density
        :param bandwidth: if None, optimize; if a float, use this value
        :return: np.array, the estimated density
        """
        if bandwidth is None:
            bandwidths = 10 ** np.linspace([-1], [1], 20).reshape(-1)
            grid = GridSearchCV(estimator=KernelDensity(kernel='gaussian'), param_grid={'bandwidth': bandwidths},
                                cv=LeaveOneOut())
            grid.fit(y.reshape(-1, 1))
            kde = KernelDensity(kernel='gaussian', **grid.best_params_).fit(y.reshape(-1, 1))
            self.bandwidth = grid.best_params_['bandwidth']
        else:
            kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(y.reshape(-1, 1))
        return np.exp(kde.score_samples(x)).reshape(-1, 1)

    def plot_results(self, w0, w1, p0, p1, result_kde, result_cgi):
        """
        Plots the two densities, their intersection
        and all individual work values at the bottom;
        also writes the precise solution to a file
        (this is admittedly a bit confusing)
        :param w0: np.array, work values for A->B
        :param w1: np.array, -work values for B->A
        :param p0: np.array, X:Y two columns for the first density
        :param p1: np.array, X:Y two columns for the second density
        :param result_kde: float, KDE result
        :param result_cgi: float, CGI result
        :return: None
        """
        fig, axes = plt.subplots(2, 1)
        axes[0].plot(*p0.T, c='C0')
        axes[0].plot(*p1.T, c='C1')
        with open('dG_{}.dat'.format(self.name), 'w') as outfile:
            axes[0].axvline(result_kde, ls='--', c='k', lw=3)
            outfile.write(str(result_kde) + '\n')
            outfile.write(str(result_cgi) + '\n')
        axes[0].fill_between(*p0.T, color='C0', alpha=0.25)
        axes[0].fill_between(*p1.T, color='C1', alpha=0.25)
        axes[0].plot(*p1.T, c='C1')
        axes[1].scatter(w0, np.zeros(len(w0)), c='C0')
        axes[1].scatter(w1, np.zeros(len(w1)), c='C1')
        axes[0].set_xlim(np.min(p0[:, 0]), np.max(p0[:, 0]))
        axes[1].set_xlim(np.min(p0[:, 0]), np.max(p0[:, 0]))
        datarange = np.max(p0[:, 0]) - np.min(p0[:, 0])
        axes[1].set_ylim(-datarange / 2, datarange / 2)
        axes[1].get_yaxis().set_visible(False)
        axes[0].get_xaxis().set_visible(False)
        axes[1].set_aspect(0.05)
        axes[1].spines['top'].set_visible(False)
        plt.subplots_adjust(hspace=-0.47)
        plt.savefig('{}.svg'.format(self.name))
        plt.close()

    @staticmethod
    def read_results(crook):
        crook.analyze_me()


class Crooks:
    def __init__(self, master, num, initlambda):
        """
        A single worker instance
        :param master: a CrooksPool instance
        :param num: ID of the worker
        :param initlambda: initial alchemical lambda (0 or 1)
        """
        self.id = num
        self.master = master
        self.initlambda = initlambda
        self.work = 0
        self.dir = self.master.dirs_0[self.id] if self.initlambda == 0 else self.master.dirs_1[self.id]

    def analyze_me(self):
        """
        Reads the .xvg file, removes potential duplicates (if the
        job was restarted), and integrates dH/dl to calculate work
        :return: None
        """
        xvgfile = ''
        xvgfiles = glob.glob('{}/*xvg'.format(self.dir))
        if len(xvgfiles) == 0:
            raise FileNotFoundError('no dhdl .xvg file found in {}'.format(self.dir))
        else:
            for f in xvgfiles:
                if any(['title "dH' in line for line in open(f)]):
                    xvgfile = f
                    break
            if not xvgfile:
                raise FileNotFoundError('no dhdl .xvg file found in {}'.format(self.dir))
        dhdl = np.loadtxt(xvgfile, comments=['#', '@'])
        assert int(dhdl[-1, 0]) == int(self.master.sim_length)
        dhdl_dict = {i: j for i, j in dhdl}
        dhdl = np.array([dhdl_dict[i] for i in sorted(list(dhdl_dict.keys()))])  # avoid duplicates in case of restarts
        endlambda = 1 if self.initlambda == 0 else 0
        self.work = simps(dhdl, np.linspace([self.initlambda], [endlambda], len(dhdl)).reshape(-1))


if __name__ == "__main__":
    def parse_args():
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', type=int, dest='time', default=500,
                            help='length (in ps) of a single slow-growth simulation')
        parser.add_argument('-c', dest='conv', action='store_true', help='whether to analyze convergence')
        parser.add_argument('-a', type=str, dest='alias', default='free',
                            help='basename for output files, default is "free"')
        parser.add_argument('-x', type=str, dest='sel0', help='regexp selection for lambda=0 directories')
        parser.add_argument('-y', type=str, dest='sel1', help='regexp selection for lambda=1 directories')
        arguments, unknown = parser.parse_known_args()
        return arguments, {x: y for x, y in [q.split('=') for q in unknown if q.count('=') == 1]}


    args, extra = parse_args()
    pool = CrooksPool(length=args.time, convergence=args.conv, alias=args.alias, sel_d0=args.sel0, sel_d1=args.sel1)
    pool.analyze()
