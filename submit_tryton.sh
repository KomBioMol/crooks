#SBATCH -J crooks
#SBATCH -N 18
#SBATCH -n 432
#SBATCH --ntasks-per-node=24
#SBATCH --time=16:00:00
#SBATCH -p batch_16h

module purge
module load tryton/gromacs/5.1.4-plumed.2.2.3
module load tryton/mpi/intel/2018

cd $SLURM_SUBMIT_DIR

mpiexec -n 432 /users/kdm/miloszw/anaconda3/bin/python -m mpi4py.futures ./crooks.py -f dyn.gro --fxtc dyn0.xtc --gxtc dyn1.xtc -p topol_nacl.top -n 215 -t 500 -g /users/kdm/miloszw/gmx504-plumed22-nompi/bin/gmx --maxwarn 3 -a nacl