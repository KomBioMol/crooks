# crooks

`crooks` is a Python script that performs Gromacs-based free energy estimation
based on the Crooks Fluctuation Theorem. The purpose of the script is
to generate ensembles of fast-growth trajectories, and analyze the
distribution of alchemical works to estimate the associated free energy change.

### Prerequisites

The script makes use of several popular libraries, including
`mdtraj`, `mpi4py`, `sklearn`, `scipy`, `numpy` and `matplotlib`. Any
missing dependencies can be installed with `pip`:

```
pip install mdtraj mpi4py sklearn scipy numpy matplotlib
```

#### Standard usage

The standard run consists of a number of non-equilibrium trajectories
with values of `lambda` linearly changing from 0 to 1 or from 1 to 0.
To run the script, one needs to provide the alchemical topology
using `-p`, a compatible structure using `-f`, and two trajectories:
one containing samples from the equilibrium ensemble corresponding to
lambda=0 (`--fxtc`), and another representative of lambda=1 (`--gxtc`).
Ideally, these trajectories should be obtained using any enhanced
sampling method, e.g. bias exchange metadynamics, to make individual
seeding frames as independent as possible.

By default, 100 jobs will be run, for 500 ps each; these defaults can
be changed with options `-n` and `-t`, respectively. The gmx executable
(by default `gmx_mpi`) can be specified using `-g`, and the maximal
number of warnings for `gmx grompp` can be set with `--maxwarn`
(by default 1).

The script runs Gromacs in parallel, so it's best to run it as a batch
job on a cluster. Several submission scripts, optimized for individual 
clusters, are included in the package. 

A sample execution command looks as follows:

```
mpiexec -n 168 /net/people/$USER/anaconda3/bin/python -m mpi4py.futures ./crooks.py -f ../mygro.gro --fxtc ../state0.xtc --gxtc ../state1.xtc -p ../topol.top -n 150 -t 1000 -g gmx --maxwarn 2
```

Note that with `mpi4py`, at least one process has to orchestrate the
workers, so the number of processes set with `mpiexec -n ...` is greater
by 1 than the number of simulation slots available. For this reason,
to run e.g. on 2 nodes with 24 CPUs per node, it might be more efficient
to set `mpiexec -n 48` and then `./crooks.py -n 23` (this will spawn 46
`mdrun` jobs + 1 'master' process, allowing all simulations to finish
more or less simultaneously).

### Output files

`crooks.py` plots the resulting distributions along with an estimation
of the free energy difference. Also, individual values of work and
distribution of works are written to files `work_X.dat` and `prob_X.dat`,
where `X` is the initial lambda value. The free energy estimates (in
kJ/mol) are written to the file `dG_S.dat`, where `S` is the selected
basename, as specified with `-a`.

To give a rough idea of overlap between the two distributions, a
normalized similarity coefficient (calculated as the Bhattacharyya
coefficient) is printed to the file `normalized_overlap_S.dat`. As a
rule of thumb, values higher than 0.1 would be considered trustworthy,
although visual inspection is recommended in all "difficult" cases
anyway.

### Analysis

There are checks in place to avoid re-running jobs that were already
completed; however, to make things faster, one can use the `--norun`
flag to perform analysis only. A sample command line would look as
follows:

```
python ./crooks.py -n 150 --norun
```

### Additional analysis tools

To analyze convergence with respect to the number of runs, add the `-c`
flag to obtain a plot of free energy estimates (along with error bars)
for different run numbers, generated through bootstrapping. To get a
very detailed info on the bootstrap analysis, add `--debug` to print
all intermediate (subsampled) probability densities, along with the
respective KDE and CGI estimates of dG (included in the filename, in
kJ/mol, in this exact order).

### Issues

+ If your run hasn't converged, there are several ways to fix it:
  + you can add more windows: this is trivial and only requires running
  the script again with the `--offset` parameter, indicating how many
  runs have been completed so far (`crooks` will start numbering the
  new directories from the next index, e.g. with `--offset 215` and
  `-n 215` new directories will be `run215_l0`, `run216_l0` all the way
  to `run429_l1`) - note that **all** results will still be considered
  for analysis.
  + you can extend the runs (more robust for "hard" cases, i.e., with
  big changes); unfortunately, here the run has to be repeated from
  scratch, since a different pulling rate has to be used.
  + if the failure results from inadequate sampling, you can also think
  of using enhanced sampling to better sample the ensembles of states
  A and B.
+ If your job was terminated before completion, you can simply run it
again - Gromacs will recognize the checkpoint files and will continue
from the exact same point it finished at previously.
+ If your run is failing, check any of the `gmp.log` files in run
directories to see if the number of warnings was not exceeded; if this
is the case, set e.g. `--maxwarn 3` to allow for up to 3 `grompp` warnings.

If you encounter any other issues not listed here, do not hestitate
to report a bug or contact me directly at `milafternoon@gmail.com`.
