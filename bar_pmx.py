import numpy as np


class BAR:
    def __init__(self, wf, wr, T):
        self.kb = 0.00831447215  # kJ/(K*mol)
        self.wf = np.array(wf)
        self.wr = np.array(wr)
        self.T = float(T)
        self.nboots = nboots
        self.nblocks = nblocks
        self.nf = len(wf)
        self.nr = len(wr)
        self.beta = 1./(self.kb*self.T)
        self.M = self.kb * self.T * np.log(float(self.nf) / float(self.nr))
        self.dg = self.calc_dg(self.wf, self.wr, self.T)

    @staticmethod
    def calc_dg(wf, wr, T):
        nf = float(len(wf))
        nr = float(len(wr))
        beta = 1./(self.kb*T)
        M = self.kb * T * np.log(nf/nr)

        def func(x, wf, wr):
            sf = 0
            for v in wf:
                sf += 1./(1+np.exp(beta*(M+v-x)))
            sr = 0
            for v in wr:
                sr += 1./(1+np.exp(-beta*(M+v-x)))
            r = sf-sr
            return r**2

        avA = np.average(wf)
        avB = np.average(wr)
        x0 = (avA+avB)/2.
        dg = fmin(func, x0=x0, args=(wf, wr), disp=0)
        return float(dg)

