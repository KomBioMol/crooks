#!/bin/bash -l
#SBATCH -J crooks
## node number (make consistent with all '18's below)
#SBATCH -N 18
## runtime
#SBATCH --time=12:00:00
#SBATCH --qos=bsc_ls

## !!!! remember to include nodes.py in the current directory
cd $SLURM_SUBMIT_DIR

module purge
module load intel/2018.4
module load mkl/2018.4
module load gromacs/2018.3-no-mpi
module load python

nodes=18
echo $SLURM_JOB_NODELIST
python ./nodes.py $SLURM_JOB_NODELIST  
offset=0
for i in $(python3 ./nodes.py $SLURM_JOB_NODELIST); do
  srun --nodelist=$i --nodes=1 python ./crooks.py -f dyn.gro --fxtc dyn0.xtc --gxtc dyn1.xtc -p topol_nacl.top -n 23 -t 1000 -g gmx --maxwarn 2 -a dsDNA_apt_nacl --offset $offset --nompi --noanalysis --nmax $[${nodes}*23] &> SLURM_$i.log &
  echo $i
  offset=$[$offset+23]
done
#
wait

python ./crooks.py -n $[23*${nodes}] -a nacl --norun